$(document).ready( () => {

    $("#hamburger-menu").on("click", () => {
        if ($(window).width() < 768) {
            $("#hamburger-menu").toggleClass("menu-open mobile");
            $("nav#mobile").animate({
                width: "toggle"
            });
        } else {
            $("#hamburger-menu").toggleClass("menu-open");
            $("nav#desktop #nav-bg").slideToggle();
            $("nav#desktop ul").slideToggle();
        }
    });

    const popupUri = "solid_login_popup.html";
    const data_manager = new DataManager();

    $("#login").on("click", () => {
        solid.auth.popupLogin({ popupUri });
    });

    $(".logout").on("click", () => {
        solid.auth.logout();
    });

    solid.auth.trackSession(async function(session) {
        const logged_in = !!session;

        if (logged_in && window.location.href.indexOf("login") != -1) {
            // wenn der Nutzer eingeloggt ist, sich aber trotzdem auf der Login-Seite befindet, wird er zur Übersichts-/Startseite geleitet
            document.location.replace("/");
        } else if (!logged_in) {
            // wenn der Nutzer nicht eingeloggt ist, werden keine Daten geladen
            if (window.location.href.indexOf("login.html") == -1) {
                // zudem wird er zur Login-Seite geleitet, wenn er sich nicht bereits dort befindet
                document.location.replace("/login.html");
            }
            return;
        }

        const user = session.webId;
        data_manager.loadPageData(user);
    }); // end trackSession

    // Editier/Abbrechen Button
    $("#edit-meeting").on("click", () => {
        let type = $("#gathering-card #edit-meeting i:not(.d-none)").attr("class");
        
        if (type.indexOf("start") != -1 ) {
            GUI.enterEditMode();
        } else {
            $("#confirm-exit-edit-mode").modal("show");
        }      
    });

    // Abbrechen bestätigen Button
    $("#confirm-cancel").on("click", () => {
        $("#confirm-exit-edit-mode").modal("hide");
        cancelEditing();
    });

    // Create-new Abbrechen Button
    $("#cancel-gathering").on("click", () => {
        $("#confirm-exit-create").modal("show");
    });

    // Create-new Abbrechen bestätigen Button
    $("#confirm-cancel-create").on("click", () => {
        $("#confirm-exit-create").modal("hide");
        document.location.replace("/");
    });

    // Speichern Button
    $("#save-gathering, #confirm-save").on("click", () => {
        $("#confirm-exit-edit-mode").modal("hide");
        saveChanges();
    });

    // Löschen Button
    $("#delete-gathering").on("click", () => {
        $("#confirm-delete-gathering").modal("show");
    });

    // Löschen bestätigen Button
    $("#confirm-delete").on("click", () => {
        $("#confirm-delete-gathering").modal("hide");
        deleteGathering();
    });

    function cancelEditing() {
        GUI.exitEditMode(true);
    } // end cancelEditing

    function saveChanges() {
        $("#error-modal").modal("hide");
        $("#modal-update-loader").modal({
            backdrop: "static",
            keyboard: false
        });
        data_manager.editMeeting(GUI.extractMeetingData())
            .then( (response) => {
                setTimeout( () => {
                    hideModal("#modal-update-loader");
                }, 300);
                if (response.type == "created")
                    document.location.replace("/gathering.html?gathering=" + encodeURIComponent(response.redirect_uri));
                else
                    GUI.exitEditMode();
            })
            .catch( (err) => {
                setTimeout( () => {
                    hideModal("#modal-update-loader");
                }, 300);
                $("#error-modal .error-msg").text(err);
                setTimeout( () => { 
                    $("#error-modal").modal("show");
                }, 300);
                if (err) console.error(err);
            });
    } // end saveChanges

    function deleteGathering() {
        $("#error-modal").modal("hide");
        $("#modal-update-loader").modal({
            backdrop: "static",
            keyboard: false
        });
        data_manager.deleteMeeting()
        .then( (response) => {
            $("#modal-update-loader").modal("hide");
            console.log(response.url + "got successfully deleted");
            document.location.replace("/");
        })
        .catch( (err) => {
            console.error(err);
            $("#modal-update-loader").modal("hide");
            $("#error-modal .error-msg").text(err);
            $("#error-modal").modal("show");
        });
    } // end deleteGathering

    function hideModal(selector) {
        if (!selector) return false;

        let hidden = ($(selector).css("display") == "none");
        
        if (!hidden) {
            $(selector).modal("hide");
            setTimeout( () => {
                hideModal(selector)
            }, 300);
        } else {
            return true;
        }
    } // end hideModal

}); // end ready




// node-sass --watch --recursive --output resources/styles/css --source-map true --source-map-contents resources/styles/scss