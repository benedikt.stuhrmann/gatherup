/**
 * Übernimmt das Laden von relevanten Daten und kümmert sich zudem um deren Darstellung. 
 */
class DataManager {

    /**
     * Erstellt einen neuen Data Manager.
     * @param {object} store Store, welcher vom DataManager genutzt werden soll (Optional: sonst wird automatisch ein neuer Store erstellt)
     * @param {object} fetcher Fetcher, welcher vom DataManager genutzt werden soll (Optional: sonst wird automatisch ein neuer Fetcher erstellt)
     * @param {object} updater UpdateManager, welcher vom DataManager genutzt werden soll (Optional: sonst wird automatisch ein neuer UpdateManager erstellt)
     */
    constructor(store, fetcher, updater) {
        this.timeout = 5000;
        this.store = (store) ? store : $rdf.graph();
        this.fetcher = (fetcher) ? fetcher : new $rdf.Fetcher(this.store, this.timeout);
        this.updater = (updater) ? updater : new $rdf.UpdateManager(this.store);

        this.logged_in_user = null;
        this.meeting = null;
    } // end constructor

    /**
     * Lädt alle benötigten Daten für die Seite, auf welcher sich der Nutzer gerade befindet.
     * @param {string} user Eingeloggter Nutzer
     */
    loadPageData(user) {
        this.logged_in_user = user;

        if (window.location.pathname == "/")
            this.loadOverviewPageData();
        else if (window.location.pathname == "/profile.html")
            this.loadProfilePageData();
        else if (window.location.pathname == "/gathering.html")
            this.loadCreateEditPageData();
    } // end loadPageData

    /**
     * Lädt alle benötigten Daten für die Übersichtsseite (Home) und stellt diese dar.
     */
    async loadOverviewPageData() {
        let data = {};
    
        await this.fetcher.load(this.logged_in_user);
    
        // Foto
        const photo = this.store.any($rdf.sym(this.logged_in_user), NS.VCARD("hasPhoto"));
        if (photo) data.user_profile_pic = photo.value;

        GUI.update(data);

        // Meetings
        await this.loadMeetings($rdf.sym(this.logged_in_user));
        GUI.hideMeetingLoadingSpinner(700);
    } // end loadOverviewPageData

    /**
     * Lädt alle benötigten Daten für die Profilseite des Nutzers und stellt diese dar.
     */
    async loadProfilePageData() {
        let data = {};
    
        await this.fetcher.load(this.logged_in_user);
        
        // WebID
        data.user_webId = this.logged_in_user;

        let person = $rdf.sym(this.logged_in_user);
        // Foto
        const photo = this.store.any(person, NS.VCARD("hasPhoto"));
        if (photo) data.user_profile_pic = photo.value;
        // Namen
        const fullName = this.store.any(person, NS.VCARD("fn")) || this.store.any(person, NS.FOAF("name")); // wenn kein VCARD Name exisitert wird der FOAF name genommen
        if (fullName) data.user_name = fullName.value;
        // Notiz
        const note = this.store.any(person, NS.VCARD("note"));
        if (note) data.user_note = note.value;
    
        // Adressdokument finden
        let address = this.store.any(person, NS.VCARD("hasAddress"));
        if (address) {
            // Adressdokument fetchen
            await this.fetcher.load(address);
    
            // Region und Land aus Adressdokument auslesen
            let user_address = {}
            const region = this.store.any(address, NS.VCARD("region"));
            if (region) user_address.region = region.value;
            const country = this.store.any(address, NS.VCARD("country-name"));
            if (region) user_address.country = country.value;
            if (user_address.region || user_address.country) data.user_location = user_address;
        }

        // Freunde auslesen
        const friends = this.store.each(person, NS.FOAF("knows"));
        if (friends) {
            friends.forEach(async (friend) => {
                let info = {};
                // Profildokument des Freundes laden
                await this.fetcher.load(friend);

                // WebID
                info.webId = friend.value;
                // Name
                const fullName = this.store.any(friend, NS.FOAF("name"));
                if (fullName) info.name = fullName.value;
                // Foto
                const photo = this.store.any(friend, NS.VCARD("hasPhoto"));
                if (photo) info.profile_pic = photo.value;

                let update_data = {
                    user_friend: info
                }
                GUI.update(update_data);
            });

            GUI.hideFriendLoadingSpinner();
        }
    
        GUI.update(data);
    } // end loadProfilePageData

    /**
     * Lädt alle benötigten Daten für die Detailsansichtsseite eines Meetings und stellt diese dar.
     */
    async loadCreateEditPageData() {
        let data = {};
    
        await this.fetcher.load(this.logged_in_user);
    
        // Foto
        const photo = this.store.any($rdf.sym(this.logged_in_user), NS.VCARD("hasPhoto"));
        if (photo) data.user_profile_pic = photo.value;

        // Meeting
        let search = new URLSearchParams(window.location.search);
        let meeting_uri = search.get("gathering");
        if (meeting_uri) {
            // bestehendes bearbeiten
            const meeting = await this.getMeetingInfo(meeting_uri);
            if (meeting) {
                data.selected_meeting = meeting;
                this.updater.addDownstreamChangeListener(meeting.doc, this.meetingChanged.bind(this));
                this.meeting = meeting;
            }
        } else {
            // neues erstellen
            let public_folder_uri = await this.getPublicFolderURI(this.logged_in_user);
            GUI.enterEditMode(true, public_folder_uri.value);
        }
    
        GUI.update(data);
    } // end loadCreateEditPageData

        /**
     * Lädt den Inhalt eines Ordners
     * @param {string} folder_uri URI des Ordners
     * @param {boolean} all Optional: Gibt an ob alles geladen werden soll, oder nur LDP("contains") (Standardmäßig: false)
     * @returns {Array} Inhalt des Ordners
     */
    async loadFolder(folder_uri, all = false) {
        //console.log("LOADING " + folder_uri + ". Containing:");
        await this.fetcher.load(folder_uri);

        let contents = [];
        if (all) {
            contents = this.store.each(folder_uri);
        }
        else {
            contents = this.store.each(folder_uri, NS.LDP("contains"));
        }

        //console.table(contents);
        return contents;
    } // loadFolder

    /**
     * Lädt alle Meetings der angegebenen Person und stellt diese dar.
     * @param {string} person
     */
    async loadMeetings(person) {
        let public_folder_uri = await this.getPublicFolderURI(person);
        // Meetings im Öffentlichen Order, sowie allen Unterordnern laden
        return await this.loadAllMeetings(public_folder_uri);
    } // end loadMeetings

    /**
     * Lädt alle Meetings aus dem angegebenen Ordner und allen möglichen Unterordnern und stellt diese dar. 
     * @param {string} folder URI des Ordners 
     */
    async loadAllMeetings(folder) {
        // Meetings aus Ordner auslesen
        await this.loadMeetingsInFolder(folder);

        // Unterordner auslesen
        let folder_contents = await this.loadFolder(folder);

        // Meetings aus Unterordnern auslesen
        let promises = [];
        for (let i = 0; i < folder_contents.length; i++) {
            promises.push(this.loadAllMeetings(folder_contents[i]));
        }

        return Promise.all(promises);
    } // end loadAllMeetings

    /**
     * Lädt alle Meetings aus dem angegebenen Ordner und stellt diese dar. 
     * @param {NamedNode} folder URI des Ordners
     */
    async loadMeetingsInFolder(folder) {
        // Ordnerinhalte auslesen
        let folder_contents = await this.loadFolder(folder);

        // Ordnerinhalte überprüfen, ob es sich um ein Meeting handelt
        for (let i = 0; i < folder_contents.length; i++) {          
            await this.fetcher.load(folder_contents[i]);
            this.checkIfMeeting(folder_contents[i]);
        }
    } // end loadMeetingsInFolder

    async checkIfMeeting(element) {
        let value = element.value;
        if (value.indexOf("index.ttl") != -1) {
            // mögliches Meeting, also fetchen
            let possible_meeting_index_ttl = value;
            await this.fetcher.load($rdf.sym(possible_meeting_index_ttl));

            // prüfen, ob es sich tatsächlich um ein Meeting handelt
            let pim_meeting = this.store.match(null, null, NS.PIM_MEETING("Meeting"), $rdf.sym(possible_meeting_index_ttl));
            if (pim_meeting.length > 0) {
                // Meetinginformationen auslesen und in der GUI darstellen
                let uri = pim_meeting[0].subject.value;
                this.getMeetingInfo(uri)
                    .then( (result) => {
                        GUI.update({meeting: result});
                    }).catch( (err) => {
                        console.log(err)
                    });
            }
        }
    } // end checkIfMeeting

    /**
     * Liest die Meeting-Details des angegebenen Meetings aus und erstellt daraus ein Meeting-Objekt.
     * @param   {string} meeting_uri URI des Meetings
     * @returns {object} Ein Meeting Objekt, welches die Detail-Informationen enthält
     */
    async getMeetingInfo(meeting_uri) {
        // Meeting fetchen
        await this.fetcher.load($rdf.sym(meeting_uri));

        // Meetingobjekt, welches Details enthält, erstellen
        let meeting = {};
        meeting.meeting = $rdf.sym(meeting_uri);

        meeting.doc = $rdf.sym(meeting_uri.substr(0, meeting_uri.indexOf("#this")));

        const dtstart = this.store.any(meeting.meeting, NS.ICAL("dtstart"));
        if (dtstart) meeting.dtstart = new Date(dtstart.value);

        const dtend = this.store.any(meeting.meeting, NS.ICAL("dtend"))
        if (dtend) meeting.dtend = dtend.value;

        const location = this.store.any(meeting.meeting, NS.ICAL("location"));
        if (location) meeting.location = location.value;

        const comment = this.store.any(meeting.meeting, NS.ICAL("comment"));
        if (comment) meeting.comment = comment.value;

        const title = this.store.any(meeting.meeting, NS.ICAL("summary"));
        if (title) meeting.title = title.value;

        const author = this.store.any(meeting.meeting, NS.DC("author"));
        if (author) meeting.author = author.value;

        const participation = this.store.any(meeting.meeting, NS.FLOW("participation"));
        if (participation) meeting.participation = participation.value;

        const participants = this.store.any(meeting.meeting, NS.PIM_MEETING("participants"));
        if (participants) meeting.participants = participants.value;
        
        return meeting;
    } // end getMeetingInfo

    async editMeeting(updated_meeting) {
        let components = [];

        if (this.meeting == null) return this.createMeeting(updated_meeting);

        // Als Meeting kennzeichnen und Basisinfos hinzufügen
        if (this.meeting.create_new) {
            components.push({
                type:  "type",
                value: $rdf.sym("http://www.w3.org/ns/pim/meeting#Meeting")
            });

            components.push({
                type:  "author",
                value: $rdf.sym(this.logged_in_user)
            });

            components.push({
                type:  "created",
                value: new Date()
            });

            components.push({
                type:  "toolList",
                value: this.meeting.meeting
            });

            components.push({
                type:  "backgroundColor",
                value: "#ddddcc"
            });
        }
        // Titel
        if (updated_meeting.title != this.meeting.title) 
            components.push({
                type:  "summary",
                value: updated_meeting.title
            });
        // Datum
        if (updated_meeting.date != this.meeting.dtstart) 
            components.push({
                type:  "dtstart",
                value: updated_meeting.date
            });
        // Ort
        if (updated_meeting.location != this.meeting.location)
            components.push({
                type:  "location",
                value: updated_meeting.location
            });
        // Beschreibung
        if (updated_meeting.comment != this.meeting.comment)
            components.push({
                type:  "comment",
                value: updated_meeting.comment
            });

        let ins = [];
        let del = [];

        await this.fetcher.load(this.meeting.meeting);

        components.forEach( (component) => {
            let namespace = this.getNamespace(component.type);

            let insertions = $rdf.st(this.meeting.meeting, namespace, component.value, this.meeting.doc);
            let deletions  = this.store.statementsMatching(this.meeting.meeting, namespace, null, this.meeting.doc);

            Array.isArray(insertions) ? ins = ins.concat(insertions) : ins.push(insertions);
            Array.isArray(deletions)  ? del = del.concat(deletions)  : del.push(deletions);
        });

        return new Promise( (resolve, reject) => {
            this.updater.update(del, ins, (uri, ok, message) => {
                if (ok) {
                    (this.meeting.create_new) ? resolve({"type": "created", "redirect_uri": this.meeting.meeting.value}) : resolve({type: "updated"});
                }
                else reject(message);
            });
        });
    } // end editMeeting

    getNamespace(component) {
        let namespace = null;

        switch (component) {
            case "type":
                namespace = NS.RDF_NS(component);
                break;
            case "author":
            case "created":
                namespace = NS.DC(component);
                break;
            case "toolList":
                namespace = NS.PIM_MEETING(component);
                break;
            case "backgroundColor":
                namespace = NS.UI(component);
                break;
            default:
                namespace = NS.ICAL(component);
        }

        return namespace;
    } // getNamespace

    deleteMeeting() {
        return new Promise( (resolve, reject) => {
            this.fetcher.webOperation("DELETE", this.meeting.doc.uri) // Dokument Löschen
            .then( (response) => {
                if (response.ok)
                    return this.fetcher.webOperation("DELETE", this.meeting.doc.uri.replace("index.ttl", "")); // Ordner Löschen
                else 
                    reject(message); 
            })
            .then( (response) => {
                if (response.ok) 
                    resolve(response.ok);
                else
                    reject(message);
            })
            .catch( (err) => {
                reject(err);
            });
        });
    } // end editMeeting

    createMeeting(meeting_data) {
        let error = "";

        if (!meeting_data.title) error += "Please add a title for your gathering. \n";
        if (!meeting_data.path)  error += "Please add a path where your gathering should be stored. \n";
        
        if (error != "") {
            return new Promise((resolve, reject) => {
                reject(error);
            });
        } else {
            let save_at = meeting_data.public_folder + meeting_data.path;
            let doc = $rdf.sym(save_at + "/index.ttl");

            this.meeting = {
                meeting: $rdf.sym(doc.value + "#this"),
                doc: doc,
                create_new: true
            };
            
            return this.fetcher.webOperation("PUT", this.meeting.doc)
                .then( (result) => {
                    console.log(result);
                    return this.editMeeting(meeting_data);
                })
                .catch( (err) => {
                    this.meeting = null;
                    console.error(err);
                    return err;
                });
        }
    } // end createMeeting

    meetingChanged() {
        console.log(this.meeting.title + " got updated!");
        this.getMeetingInfo(this.meeting.meeting.value).then( (meeting) => {
            GUI.update({selected_meeting: meeting});
        });
    } // end meetingChanged

    /**
     * Gibt die URI des öffentlichen Ordners wieder
     * @param {*} person 
     */
    async getPublicFolderURI(person) {
        let public_folder_uri;

        // URI des Öffentlicher-Ordners der Person ermitteln
        let storages = this.store.any($rdf.sym(person), NS.PIM("storage"));
        let storage_contents = await this.loadFolder(storages);
        storage_contents.forEach(content => {
            if (content.value.indexOf("//public/") != -1 ){
                public_folder_uri = content;
            }
        });

        return public_folder_uri;
    } // end getPublicFolderURI

} // end class