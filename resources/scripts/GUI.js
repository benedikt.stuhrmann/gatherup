/**
 * Hilfsklasse mit Methoden zum Aktualisieren der GUI
 */
class GUI {

    constructor() {
        this.meeting_before_edit = null;
    } // end constructor

    /**
     * Aktualisiert die GUI der Anwendung auf Basis der übergebenen Daten.
     * @param {array[object]} data 
     */
    static update(data) {
        // Profilbild  des Nutzers aktualisieren
        if (data.user_profile_pic) this.updateUserProfilePic(data.user_profile_pic);
        // Namen des Nutzers aktualisieren
        if (data.user_name) this.updateUserName(data.user_name);
        // Standort des Nutzers aktualisieren
        if (data.user_location) this.updateUserLocation(data.user_location);
        // Notiz des Nutzers aktualisieren
        if (data.user_note) this.updateUserNote(data.user_note);
        // WebID des Nutzers aktualisieren
        if (data.user_webId) this.updateUserWebId(data.user_webId);
        // Kontakte des Nutzers aktualisieren
        if (data.user_friend) this.addUserContact(data.user_friend);
        // Meetingübersicht des Nutzers aktualisieren
        if (data.meeting) this.addUserMeeting(data.meeting);
        // Meeting Detailsansicht aktualisieren
        if (data.selected_meeting) this.updateSelectedMeeting(data.selected_meeting);
        // Teilnehmer eines Meetings aktualisieren
        if (data.meeting_participant) this.addMeetingParticipant(data.meeting_participant);
    } // end update

    /**
     * Aktualisiert alle Vorkommen des Profilbildes, des eingeloggten Users, in der Anwendung.
     * @param {string} profile_pic_uri 
     */
    static updateUserProfilePic(profile_pic_uri) {
        $(".user-profile-pic").attr("src", profile_pic_uri);
    } // end updateUserProfilePic

    /**
     * Aktualisiert alle Vorkommen des Namens, des eingeloggten Users, in der Anwendung.
     * @param {string} username 
     */
    static updateUserName(username) { 
        $(".user-fullname").text(username);
    } // end updateUserName

    /**
     * Aktualisiert alle Vorkommen der WebID, des eingeloggten Users, in der Anwendung.
     * @param {string} web_id 
     */
    static updateUserWebId(web_id) {
        $(".web-id").text(web_id).attr("href", web_id);
    } // end updateUserWebId

    /**
     * Aktualisiert alle Vorkommen des Standorts, des eingeloggten Users, in der Anwendung.
     * @param {object} location 
     */
    static updateUserLocation(location) {
        let location_text = "";
        if (location.region) {
            location_text += location.region;
        }
        if (location.country) {
            if (location_text != "") {
                location_text += ", ";
            } 
            location_text += location.country;
        }
            
        $(".location").text(location_text);
    } // end updateUserLocation

    /**
     * Aktualisiert alle Vorkommen der Notiz, des eingeloggten Users, in der Anwendung.
     * @param {string} note 
     */
    static updateUserNote(note) {
        $(".note").text(note);
        $(".note").css("display", "block");
    } // end updateUserNote

    /**
     * Fügt einen Kontakt zur Kontakübersicht des Nutzers hinzu.
     * @param {object} contact 
     */
    static addUserContact(contact) {
        if (!contact.name)        contact.name = "Unknown name";
        if (!contact.profile_pic) contact.profile_pic = "resources/solid_logo.png";

        $("<div class='contact'>").append(
            $("<a href='" + contact.webId + "' target='blank'>").append(
                $("<div class='image-round'>").append(
                    $("<img class='contact-profile-pic' src='" + contact.profile_pic + "' alt=''>")
                )
            )    
        ).append(
            $("<a href='" + contact.webId + "' class='name' target='blank'>").text(contact.name)
        ).insertBefore(".contacts #loader");
    } // end addUserContact

    /**
     * Fügt einen Teilnehmer zur Kontakübersicht eines Meetings hinzu.
     * @param {object} contact 
     */
    static addMeetingParticipant(participant) {
        // TODO
    } // end addMeetingParticipant

    /**
     * Fügt ein Meeting zur Meetingübersicht des Nutzers hinzu.
     * @param {object} meeting 
     */
    static addUserMeeting(meeting) {
        meeting.dtstart = (!meeting.dtstart) ? "Unknown" : meeting.dtstart.toLocaleDateString();
        if (!meeting.location) meeting.location = "Unknown";
        
        const max_length = 275;
        if (meeting.comment.length > max_length) {
            meeting.comment = meeting.comment.substr(0, max_length) + " ...";
        }

        $("<div class='col-12 col-lg-6'>").append(
            $("<div class='card clickable gathering'>").append(
                $("<a href='/gathering.html?gathering=" + encodeURIComponent(meeting.meeting.value) + "'>").append(
                    $("<div class='main-info'>").append(
                        /*$("<div class='gathering-pic image-round hide-xs'>").append(
                            $("<img class='gathering-pic' src='resources/solid_logo.png' alt='' />")
                        )
                    ).append(*/
                        $("<div class='info'>").append(
                            $("<h2>").text(meeting.title)
                        ).append(
                            $("<div class='date-info icon-label'>").append(
                                $("<i class='material-icons'>").text("date_range")
                            ).append(
                                $("<span class='date'>").text(meeting.dtstart)
                            )
                        ).append(
                            $("<div class='location-info icon-label'>").append(
                                $("<i class='material-icons'>").text("location_on")
                            ).append(
                                $("<span class='location'>").text(meeting.location)
                            )
                        )
                    )
                ).append(
                    $("<div class='detail-info'>").append(
                        $("<p class='description'>").text(meeting.comment)
                    )
                )
            )
        ).insertAfter(".user-gatherings > div:last-of-type");
    } // end addUserMeeting

    /**
     * Aktualisiert die Detailansicht des ausgewählten Meetings.
     * @param {object} meeting 
     */
    static updateSelectedMeeting(meeting) {
        meeting.dtstart = (!meeting.dtstart) ? "Unknown" : meeting.dtstart.toLocaleDateString();
        if (!meeting.location) meeting.location = "Unknown";

        $("#gathering-card .main-info h2 a").text(meeting.title).attr("href", meeting.meeting.value);
        $("#gathering-card .main-info .date").text(meeting.dtstart);
        $("#gathering-card .main-info .location").text(meeting.location);
        $("#gathering-card .detail-info .description").text(meeting.comment);
    }

    /**
     * Blendet den Ladeindikator in der Meetingübersicht aus.
     * @param {number} delay Verzögerung in Millisekunden, bevor das Ausblenden startet
     */
    static hideMeetingLoadingSpinner(delay = 0) {
        setTimeout( () => {
            $(".user-gatherings #loader > div").slideUp(500, () => {
                $("#loader").css("display", "none");
                $("#placeholder").removeClass("d-none");
            });
        }, delay);
    } // end hideMeetingLoadingSpinner

    /**
     * Blendet den Ladeindikator für die Kontaktübersicht aus.
     * @param {number} delay Verzögerung in Millisekunden, bevor das Ausblenden startet
     */
    static hideFriendLoadingSpinner(delay = 0) {
        setTimeout( () => {
            $(".contacts #loader").fadeOut(2000);
        }, delay);
    } // end hideFriendLoadingSpinner

    /**
     * Ändert die GUI in einen Editiermodus, welcher erlaubt die einzelnen Attribute eines Meetings zu ändern.
     */
    static enterEditMode(create_new = false, public_folder_uri = null) {
        if (create_new) {
            $("#gathering-card #edit-meeting").addClass("d-none"); // Edit-Button/Cancel-Button ausblenden
            $("#actions-card .actions #delete-gathering").addClass("d-none"); // Löschen-Button ausblenden
            $("#actions-card .actions #cancel-gathering").removeClass("d-none"); // Abbrechen-Button einblenden
            $("#actions-card .actions #save-gathering").text("Create"); // "Save" in "Create" ändern

            // Input für Speicherort anpassen und anzeigen
            $("#gathering-card input[name='public-folder-path']").val(public_folder_uri);
            let span = $("<span id='temp-span' style='font-size: 20px; visibility: hidden;'>").text(public_folder_uri).insertAfter("#gathering-card");
            let width = $(span).width() + $("input[name='public-folder-path']").css("padding-left").replace("px", "") * 1; // Breite der Public-Folder-URI + Padding vom text-input
            $("#temp-span").remove();
            $("input[name='public-folder-path']").css("width", width);
            $("input[name='save-path']").css("padding-left", width);
            $("#gathering-card .path").toggleClass("d-none"); // Speicherort einblenden
        } else {            
            $("#gathering-card #edit-meeting i").toggleClass("d-none"); // Edit-Button in Cancel-Button ändern
        }

        $("#actions-card").fadeIn(); // Action-Buttons einblenden

        let meeting = {};

        // Titel
        meeting.title = $("#gathering-card h2").text();
        $("#gathering-card h2").replaceWith(
            $("<input id='edit-title' type='text' class='edit-gathering' placeholder='Add meeting title' value='" + meeting.title + "'>")
        );

        // Datum
        let date;
        if (create_new) {
            date = new Date().toLocaleDateString(); // heutiges Datum als Platzhalter
        } else {
            date = $("#gathering-card .date").text();
        }
        let parts = date.split(".");
        date = parts[2] + "-" + parts[1] + "-" + parts[0]; 
        meeting.date = new Date(date);
        $("#gathering-card .date").replaceWith(
            $("<input id='edit-date' type='date' class='edit-gathering date' value='" + date + "'>")
        );

        // Ort
        meeting.location = $("#gathering-card .location").text();
        $("#gathering-card .location").replaceWith(
            $("<input id='edit-location' type='text' class='edit-gathering location' placeholder='Add location' value='" + meeting.location + "'>")
        );

        // Beschreibung
        meeting.comment = $("#gathering-card .description").text();
        $("#gathering-card .description").replaceWith(
            $("<textarea id='edit-description' class='edit-gathering description' placeholder='Add description' value='" + meeting.comment + "' oninput='this.style.height = this.scrollHeight + \"px\"'>").text(meeting.comment)
        );

        this.meeting_before_edit = meeting;
    } // end enterEditMode

    /**
     * Ändert die GUI vom Editiermodus zurück in den Anzeigemodus eines Meetings.
     */
    static exitEditMode(discard_changes = false) {
        $("#gathering-card #edit-meeting i").toggleClass("d-none"); // Cancel-Button in Edit-Button ändern
        $("#actions-card").fadeOut(); // Buttons ausblenden

        let form_meeting_data = this.extractMeetingData();
        // Titel
        let title = (discard_changes) ? this.meeting_before_edit.title : form_meeting_data.title;
        $("#gathering-card #edit-title").replaceWith(
            $("<h2>").text(title)
        );

        // Datum
        let date = (discard_changes) ? this.meeting_before_edit.date : form_meeting_data.date;
        $("#gathering-card .date").replaceWith(
            $("<span class='date'>").text(date.toLocaleDateString())
        );

        // Ort
        let location = (discard_changes) ? this.meeting_before_edit.location : form_meeting_data.location;
        $("#gathering-card .location").replaceWith(
            $("<span class='location'>").text(location)
        );

        // Beschreibung
        let description = (discard_changes) ? this.meeting_before_edit.comment : form_meeting_data.comment;
        $("#gathering-card .description").replaceWith(
            $("<p class='description'>").text(description)
        );
    } // end exitEditMode

    static extractMeetingData() {
        let meeting = {};

        meeting.title = $("#gathering-card #edit-title").val();
        meeting.date = new Date($("#gathering-card .date").val());
        meeting.location = $("#gathering-card .location").val();
        meeting.comment = $("#gathering-card .description").val();
        meeting.public_folder = $("#gathering-card input[name='public-folder-path']").val();
        meeting.path = $("#gathering-card input[name='save-path']").val();

        return meeting;
    } // end extractMeetingData

}  // end class