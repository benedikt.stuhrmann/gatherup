const NS = {
    VCARD       : $rdf.Namespace("http://www.w3.org/2006/vcard/ns#"),
    FOAF        : $rdf.Namespace("http://xmlns.com/foaf/0.1/"),
    PIM         : $rdf.Namespace("http://www.w3.org/ns/pim/space#"),
    PIM_MEETING : $rdf.Namespace("http://www.w3.org/ns/pim/meeting#"),
    LDP         : $rdf.Namespace("http://www.w3.org/ns/ldp#"),
    RDF_NS      : $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#"),
    ICAL        : $rdf.Namespace("http://www.w3.org/2002/12/cal/ical#"),
    DC          : $rdf.Namespace("http://purl.org/dc/elements/1.1/"),
    FLOW          : $rdf.Namespace("http://www.w3.org/2005/01/wf/flow#"),
    UI          : $rdf.Namespace("http://www.w3.org/ns/ui#")
}